package com.example.cigradle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiGradleApplication {

    public static void main(String[] args) {
        SpringApplication.run(CiGradleApplication.class, args);
    }

}
